=======
sb_snippet
=======

**WARNING: This package is deprecated! Use new package vl_snippet:** https://github.com/vivazzi/vl_snippet


--------

--------

--------

sb_snippet is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds snippet (custom html, js code or prepared template) to page.


Installation
============

sb_snippet requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_snippet in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_snippet


Configuration 
=============

1. Add "sb_snippet" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_snippet',
        ...
    )

2. Run `python manage.py migrate` to create the sb_snippet models.