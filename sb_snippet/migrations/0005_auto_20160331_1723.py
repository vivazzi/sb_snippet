from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_snippet', '0004_auto_20160219_0916'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbsnippet',
            name='template',
            field=models.CharField(max_length=255, null=True, verbose_name='\u0413\u043e\u0442\u043e\u0432\u044b\u0439 \u0441\u043d\u0438\u043f\u043f\u0435\u0442', blank=True),
        ),
    ]
