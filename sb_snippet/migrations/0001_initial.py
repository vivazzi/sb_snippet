from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0003_auto_20140926_2347'),
    ]

    operations = [
        migrations.CreateModel(
            name='SBSnippet',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('title', models.CharField(help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u043c\u0438\u043d\u043a\u0435', max_length=100, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('content', models.TextField(help_text='\u041c\u043e\u0436\u043d\u043e \u0432\u0432\u043e\u0434\u0438\u0442\u044c \u043b\u044e\u0431\u043e\u0439 js \u0438 html-\u043a\u043e\u0434.', verbose_name='\u041a\u043e\u0434')),
            ],
            options={
                'db_table': 'sb_snippet',
                'verbose_name': '\u0421\u043d\u0438\u043f\u043f\u0435\u0442',
                'verbose_name_plural': '\u0421\u043d\u0438\u043f\u043f\u0435\u0442\u044b',
            },
            bases=('cms.cmsplugin',),
        ),
    ]
