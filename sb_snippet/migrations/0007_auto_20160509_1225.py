from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_snippet', '0006_auto_20160509_1123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbsnippet',
            name='is_video',
            field=models.BooleanField(default=False, help_text='\u041e\u0442\u043c\u0435\u0442\u044c\u0442\u0435, \u0435\u0441\u043b\u0438 \u0432\u044b \u0445\u043e\u0442\u0438\u0442\u0435 \u0440\u0430\u0437\u043c\u0435\u0441\u0442\u0438\u0442\u044c \u043a\u043e\u0434 \u0432\u0438\u0434\u0435\u043e.', verbose_name='\u0412\u0438\u0434\u0435\u043e?'),
        ),
    ]
