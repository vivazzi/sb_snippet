from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_snippet', '0002_auto_20150829_1658'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbsnippet',
            name='template',
            field=models.CharField(max_length=255, null=True, verbose_name='\u041f\u043e\u0434\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u043d\u044b\u0439 \u043a\u043e\u043c\u043f\u043e\u043d\u0435\u043d\u0442', blank=True),
        ),
        migrations.AlterField(
            model_name='sbsnippet',
            name='content',
            field=models.TextField(help_text='\u041c\u043e\u0436\u043d\u043e \u0432\u0432\u043e\u0434\u0438\u0442\u044c \u043b\u044e\u0431\u043e\u0439 js \u0438 html-\u043a\u043e\u0434.', null=True, verbose_name='\u041a\u043e\u0434', blank=True),
        ),
    ]
