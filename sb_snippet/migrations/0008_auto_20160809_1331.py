from django.db import models, migrations


def set_blank(apps, schema_editor):
    SBSnippet = apps.get_model('sb_snippet', 'SBSnippet')
    for obj in SBSnippet.objects.all():
        obj.content = obj.content or ''
        obj.reformed_content = obj.reformed_content or ''
        obj.template = obj.template or ''
        obj.title = obj.title or ''
        obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sb_snippet', '0007_auto_20160509_1225'),
    ]

    operations = [
        migrations.RunPython(set_blank),
        migrations.AlterField(
            model_name='sbsnippet',
            name='content',
            field=models.TextField(default='', help_text='\u041c\u043e\u0436\u043d\u043e \u0432\u0432\u043e\u0434\u0438\u0442\u044c \u043b\u044e\u0431\u043e\u0439 js \u0438 html-\u043a\u043e\u0434.', verbose_name='\u041a\u043e\u0434', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbsnippet',
            name='reformed_content',
            field=models.TextField(default='', editable=False, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbsnippet',
            name='template',
            field=models.CharField(default='', max_length=255, verbose_name='\u0413\u043e\u0442\u043e\u0432\u044b\u0439 \u0441\u043d\u0438\u043f\u043f\u0435\u0442', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbsnippet',
            name='title',
            field=models.CharField(default='', help_text='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0435\u0442\u0441\u044f \u0442\u043e\u043b\u044c\u043a\u043e \u0432 \u0430\u0434\u043c\u0438\u043d\u0438\u0441\u0442\u0440\u0430\u0442\u0438\u0432\u043d\u043e\u0439 \u0447\u0430\u0441\u0442\u0438', max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=False,
        ),
    ]
