from django.db import models, migrations


def migrate_content(apps, schema_editor):
    SBSnippet = apps.get_model('sb_snippet', 'SBSnippet')
    for obj in SBSnippet.objects.all():
        obj.reformed_content = obj.content
        obj.save()


class Migration(migrations.Migration):

    dependencies = [
        ('sb_snippet', '0005_auto_20160331_1723'),
    ]

    operations = [
        migrations.AddField(
            model_name='sbsnippet',
            name='is_video',
            field=models.BooleanField(default=False, help_text='\u041e\u0442\u043c\u0435\u0442\u044c\u0442\u0435, \u0435\u0441\u043b\u0438 \u0432\u044b \u0445\u043e\u0442\u0438\u0442\u0435 \u0440\u0430\u0437\u043c\u0435\u0441\u0442\u0438\u0442\u044c \u043a\u043e\u0434 \u0432\u0438\u0434\u0435\u043e', verbose_name='\u0412\u0438\u0434\u0435\u043e?'),
        ),
        migrations.AddField(
            model_name='sbsnippet',
            name='reformed_content',
            field=models.TextField(null=True, editable=False, blank=True),
        ),
        migrations.RunPython(migrate_content),
    ]
