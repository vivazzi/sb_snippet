from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_snippet', '0008_auto_20160809_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbsnippet',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_snippet_sbsnippet', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]
