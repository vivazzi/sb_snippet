from django.utils.translation import gettext as _
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from sb_core.forms import TemplateForm
from sb_core.mixins import RenderTemplateMixin
from sb_core.constants import BASE
from sb_snippet.models import SBSnippet


class SBSnippetPlugin(RenderTemplateMixin, CMSPluginBase):
    module = BASE
    model = SBSnippet
    name = _('Snippet')
    form = TemplateForm
    render_template = 'sb_snippet/sb_snippet.html'
    allow_children = True
    text_enabled = True

    fields = ('title', 'content', ('tag_id', 'tag_classes', 'is_video'), 'template')


plugin_pool.register_plugin(SBSnippetPlugin)
