import re

from django.db import models
from django.utils.translation import gettext_lazy as _, pgettext_lazy
from cms.models import CMSPlugin
from django.utils.html import escape
from django.utils.safestring import mark_safe
from sb_core.constants import title_ht
from sb_core.fields import TemplateField
from sb_core.template_pool import template_pool


class SBSnippet(CMSPlugin):
    title = models.CharField(pgettext_lazy('sbl title', 'Title'), max_length=100, blank=True, help_text=title_ht)
    content = models.TextField(_('Code or data'), blank=True,
                               help_text=_('You can insert html, javascript code or data, if you use template'))
    reformed_content = models.TextField(blank=True, editable=False)

    is_video = models.BooleanField(_('Is video?'), default=False,
                                   help_text=_('Select if you want to insert video (from youtube)'))

    tag_id = models.CharField('ID', max_length=100, help_text=_('For example, useful for anchor'), blank=True)
    tag_classes = models.CharField(_('Classes'), max_length=200, help_text=_('Separate with blank'), blank=True)

    template = TemplateField(_('Template'), blank=True)

    def get_content(self):
        return mark_safe(self.reformed_content)

    def __str__(self):
        res = ''
        res += self.title if self.title else escape(self.content[:50])

        if self.template:
            res += template_pool.obj_template_str(self, res)

        return res

    def save(self, no_signals=False, *args, **kwargs):
        if self.is_video:
            string = re.findall(r'src=".+?"', self.content)
            if string:
                string = string[0]
                url = string[5:-1]
                i = url.find('?')
                if i != -1:
                    url = url[:i]
                self.reformed_content = url
            else:
                self.reformed_content = ''
        else:
            self.reformed_content = self.content
        super(SBSnippet, self).save(no_signals, *args, **kwargs)

    class Meta:
        db_table = 'sb_snippet'
        verbose_name = _('Snippet')
        verbose_name_plural = _('Snippets')
